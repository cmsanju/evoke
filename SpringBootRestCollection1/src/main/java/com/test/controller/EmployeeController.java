package com.test.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.test.model.Employee;
import com.test.model.Employees;
import com.test.service.EmpService;

@RestController
public class EmployeeController {
	
	@Autowired
	private EmpService empService;
	
	//Read employee
	@GetMapping(value="/reademp", produces = "application/json")
	public Employees getAllEmployees()
	{
		return empService.getAllEmployees();
	}
	//Create employee
	@PostMapping(value="/addemp", consumes = "application/json")
	public Employees addEmployee(@RequestBody Employee emp)
	{
		empService.addEmployee(emp);
		
		return empService.getAllEmployees();
	}
	
	//update employee
	@PutMapping(value="/updateemp/{id}", consumes = "application/json")
	public Employees updateEmployee(@PathVariable("id") Integer id, @RequestBody Employee emp)
	{
		emp.setId(id);
		
		empService.updateEmployee(emp);
		
		return empService.getAllEmployees();
	}
	
	//Delete employee
	@DeleteMapping(value = "/deleteemp/{id}", produces = "application/json")
	public Employees deleteEmployee(@PathVariable("id") Integer id)
	{
		empService.deleteEmployee(id);
		
		return empService.getAllEmployees();
	}
}
