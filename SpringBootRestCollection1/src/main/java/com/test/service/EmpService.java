package com.test.service;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.test.model.Employee;
import com.test.model.Employees;

@Repository
@Service
public class EmpService 
{
	private static Employees emp = new Employees();
	
	static
	{
		emp.getEmpList().add(new Employee(101, "Hero", "PWC"));
		emp.getEmpList().add(new Employee(102, "King", "E Y"));
		emp.getEmpList().add(new Employee(103, "Rakesh", "Dell"));
		emp.getEmpList().add(new Employee(104, "Shivam", "E Y"));
		emp.getEmpList().add(new Employee(105, "Gupta", "ITC"));
	}
	
	//read all employee list
	public Employees getAllEmployees()
	{
		return emp;
	}
	//creating new employee record
	public void addEmployee(Employee obj)
	{
		emp.getEmpList().add(obj);
	}
	//update employee
	public String updateEmployee(Employee eobj)
	{
		for(int i = 0; i < emp.getEmpList().size(); i++)
		{
			Employee obj = emp.getEmpList().get(i);
			
			if(obj.getId().equals(eobj.getId()))
			{
				emp.getEmpList().set(i, eobj);
			}
		}
		
		return "the given id is not available";
	}
	//Delete employee
	public String deleteEmployee(Integer id)
	{
		for(int i = 0; i<emp.getEmpList().size(); i++)
		{
			Employee obj = emp.getEmpList().get(i);
			
			if(obj.getId().equals(id))
			{
				emp.getEmpList().remove(i);
			}
		}
		
		return "the given id is not available";
	}
}
